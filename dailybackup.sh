#!/bin/bash

############################################################################
# Backup Script
# Neil Vaytet (ENS Lyon) - 03/2014
############################################################################

# The source directories:
SRC="/home/user/work /home/user/Documents /home/user/Downloads";

# Backup frequency: (YY:MM:DD:hh:mm:ss)
FBACKUP="00:00:01:00:00:00";

# Backup time offset: (YY:MM:DD:hh:mm:ss)
OBACKUP="00:00:00:04:00:00";

# Backup locations:
REMOTEHOST[0]="";
BACKPATH[0]="/address/of/hard/drive/one/";

REMOTEHOST[1]="";
BACKPATH[1]="/address/of/hard/drive/two/";

REMOTEHOST[2]="user@remote.server.fr";
BACKPATH[2]="/home/user/backup/";

# Add more locations here
# REMOTEHOST[3]="user@remote2.server.fr";
# BACKPATH[3]="/home/user/backup2/";

# Perform a full backup every N backups
# Set to 1 if you want to perform hard backups every time
# Set to 0 if you only want to create incremental backups
NFULLBACKUP=10;

# Log file
LOGFILE="backup.log";

############################################################################

# Count number of different backups
NBACKUP=${#BACKPATH[@]};

# Make sure that path of backups have "/" at the end
for ((n=0;n<$NBACKUP;n++)); do
    length=${#BACKPATH[${n}]};
    char=${BACKPATH[${n}]:$length-1:1};
    if [ "$char" != "/" ] ; then
       BACKPATH[${n}]="${BACKPATH[${n}]}/";
    fi
done

# Set up variables
hline="============================================================";

# Set time interval for checks (in seconds)
pause=100;

# Perform full backup to begin with by default
FULLBACKUP=true;

############################################################################

# Find next backup time:

# Decompose frequencies and offsets into arrays
fbackupstring=$(echo $FBACKUP | sed 's/:/ /g');
frequencies=( $fbackupstring );
obackupstring=$(echo $OBACKUP | sed 's/:/ /g');
offsets=( $obackupstring );

# Find lowest time denominator
for ((i=0;i<6;i++)); do
    if [ ${frequencies[${i}]} -gt 0 ] ; then
        unit=$i;
        break;
    fi
done

# Determine reference time:
# If time now is 15:15 and a backup is performed every hour, reference time is 15:00
TIMENOW=$(date "+%Y %m %d %H %M %S");
REFTIME=( $TIMENOW );
for ((i=5;i>$unit;i--)); do
    REFTIME[${i}]="00";
done

# Remove leading zeros for arithmetic operations
for ((i=0;i<6;i++)); do
   if [ "${REFTIME[i]:0:1}" == "0" ] ; then
      REFTIME[i]="${REFTIME[i]:1:1}";
   fi
   if [ "${offsets[i]:0:1}" == "0" ] ; then
      offsets[i]="${offsets[i]:1:1}";
   fi
done

# Offset current time to find next backup time
for ((i=0;i<6;i++)); do
    OFFTIME[i]=$((${REFTIME[i]}+${offsets[i]}));
done
TNEXTBACKUP=$(date -d "${OFFTIME[0]}-${OFFTIME[1]}-${OFFTIME[2]} ${OFFTIME[3]}:${OFFTIME[4]}:${OFFTIME[5]}" +%s)

# Unit description
UNITDESCRIPTION[0]="year";
UNITDESCRIPTION[1]="month";
UNITDESCRIPTION[2]="day";
UNITDESCRIPTION[3]="hour";
UNITDESCRIPTION[4]="minute";
UNITDESCRIPTION[5]="second";

# Determine whether we are to backup in current time interval or the next
TIMENOW=$(date +%s);
if [ $TIMENOW -gt $TNEXTBACKUP ] ; then
    TNEXTBACKUP=$(date -d "${OFFTIME[0]}-${OFFTIME[1]}-${OFFTIME[2]} ${OFFTIME[3]}:${OFFTIME[4]}:${OFFTIME[5]} 1 ${UNITDESCRIPTION[${unit}]}" +%s);
fi

############################################################################

print_message=true;

# Now begin infinite loop which will create backups
while true ; do

    # Convert next backup time seconds into more readable form
    if $print_message ; then
       echo "Will perform next backup at $(date -d "1970-01-01 UTC ${TNEXTBACKUP} second")" >> $LOGFILE;
    fi
    print_message=false;

    # Check whether it is time to backup
    TIMENOW=$(date "+%s");

    if [ $TIMENOW -ge $TNEXTBACKUP ]; then
    
        print_message=true;

        # Todays date for directory name:
        DAY0=$(date "+%Y-%m-%d-%H-%M-%S");
        
        # Determine backup count to see if we need to perform a full backup
        PREVBCKPCOUNT=$(grep "Backup counter:" $LOGFILE | tail -1 | cut -d "|" -f2);
        if [ ${#PREVBCKPCOUNT} -eq 0 ]; then
            PREVBCKPCOUNT=0;
        fi
        BACKUPCOUNT=$(($PREVBCKPCOUNT + 1));
        if [ $NFULLBACKUP -eq 0 ]; then
            FULLBACKUP=false;
        else
            if [ $BACKUPCOUNT -gt $NFULLBACKUP ]; then
                BACKUPCOUNT=1;
                FULLBACKUP=true;
            else
                FULLBACKUP=false;
            fi
        fi
        BCKPREMAINING=$(($NFULLBACKUP - $BACKUPCOUNT));

        # Loop over all backups
        for ((n=0;n<$NBACKUP;n++)); do

            length=${#REMOTEHOST[${n}]};
            if [ ${length} -gt 0 ]; then
                REMOTE[${n}]=true;
                SEPARATOR=":";
            else
                REMOTE[${n}]=false;
                SEPARATOR="";
            fi

            m=$(($n + 1));

            echo $hline >> $LOGFILE;
            echo "Performing backup ${m} for date: ${DAY0}" >> $LOGFILE;
            echo "Backup location: ${REMOTEHOST[${n}]}${SEPARATOR}${BACKPATH[${n}]}" >> $LOGFILE;
            if [ $NFULLBACKUP -eq 0 ]; then
                echo "Backup counter:|${BACKUPCOUNT}|, currently only performing incremental backups" >> $LOGFILE;
            else
                if $FULLBACKUP; then
                    echo "Backup counter:|${BACKUPCOUNT}|, performing a full backup" >> $LOGFILE;
                else
                    echo "Backup counter:|${BACKUPCOUNT}|, ${BCKPREMAINING} more until full backup" >> $LOGFILE;
                fi
            fi

            # Target directory
            TRG="${REMOTEHOST[${n}]}${SEPARATOR}${BACKPATH[${n}]}${DAY0}";

            # Check if target directory already exists
            do_backup=true;
            if ${REMOTE[${n}]} ; then
                if ssh ${REMOTEHOST[${n}]} test -d "$TRG" ; then
                    do_backup=false;
                fi
            else
                if test -d "$TRG" ; then
                    do_backup=false;
                fi
            fi

            if $do_backup ; then # Only do backup if target directory does not already exist

                # Only use link directory if performing an incremental backup
            
                if $FULLBACKUP; then
                    LNKSTRING="";
                else
                    # Find the link destination: select last directory
                    if ${REMOTE[${n}]} ; then
                        LNK=$(ssh ${REMOTEHOST[${n}]} ls ${BACKPATH[${n}]} | tail -1)
                    else
                        LNK=$(ls ${BACKPATH[${n}]} | tail -1);
                    fi
                    
                    LNK=${BACKPATH[${n}]}${LNK};
                    echo "Using link directory: $LNK" >> $LOGFILE;
                    LNKSTRING="--link-dest=$LNK";
                fi

                # The rsync options:
                OPT="-ah --stats --delete $LNKSTRING";

                # Execute the backup with rsync
                { rsync $OPT $SRC $TRG >> $LOGFILE; } 2>> $LOGFILE;

            fi

        done

        echo $hline >> $LOGFILE;
        echo $hline >> $LOGFILE;

        # Update next backup time
        TNEXTBACKUP=$(date -d "1970-01-01 UTC ${TNEXTBACKUP} second ${frequencies[0]} year ${frequencies[1]} month ${frequencies[2]} day ${frequencies[3]} hour ${frequencies[4]} minute ${frequencies[5]} second" +%s);

    fi

    # Pause so that script is not checking constantly
    sleep ${pause};

done

exit;
