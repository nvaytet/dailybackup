# Description #

Daily backup (for LINUX/MAC) is a small bash script that creates daily incremental backups of your files on any number of local or remote locations. It uses rsync to make new copies of only the files that are new/have changed. It creates daily snapshots of your home area (or any other folders) into a new directory which is named after the current date.

# How to use the backup script #

The current version requires to have rsync installed.
You can run the script manually from a terminal or include it in the list of your
startup applications (I would not include it in your .bashrc as it will launch a
new instance every time you open a new terminal tab/window). I also recommend
running it with "nohup ./dailybackup.sh &" in case you close the terminal window you
launched from. You can also tell you machine to run it on boot/login.

- You have to specify at the start of the script which files/folders you want backing up.
- Next, you need to enter the backup frequency and time offset: if you want to make daily
  backups at 04h00, set FBACKUP="00:00:01:00:00:00" and OBACKUP="00:00:00:04:00:00" (the
  numbers stand for YY:MM:DD:hh:mm:ss). You can of course chose any frequency you like,
  say every 4 hours.
- You then have to indicate how many backups you want to perform, including a remote
  server and backup location for each backup (if backups are local, just leave the
  REMOTEHOST blank).

For remote backups, you need to have generated a ssh key so that the script does not
have to ask for password to connect to the remote host.

The script will look back for previous backups and will use the most recent one as a
basis for linking old files that have not changed. Only new files will be copied, thus
limiting the size of the backups. The original copy will automatically be created the
first time the script is run, and will perform linking from there onwards.

Each backup will be a snapshot of your files at the time of backup. rsync's use of hard
links makes the backed up directories look exactly like the original ones, with no sign
of the linking that is performed behind the scenes. The incremental nature of the backups
should ensure that the total size of your backed up folders should be only slightly larger
than the single original backup. If you see that the total size of your backups is doubled
the second day and tripled the third, then there is a configuration problem.